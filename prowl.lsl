///////////////////////////////////////////////////////////////////////////
//  Copyright (C) Wizardry and Steamworks 2011 - License: GNU GPLv3      //
//  Please see: http://www.gnu.org/licenses/gpl.html for legal details,  //
//  rights of fair usage, the disclaimer and warranty conditions.        //
///////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
//                   CONFIGURATION                      //
//////////////////////////////////////////////////////////

string PROWL_API_KEY = "6f5902ac237024bdd0c176cb93063dc4";
string PROWL_APPLICATION = "Prowler";
string PROWL_EVENT = "Tickled!";

//////////////////////////////////////////////////////////
//                      INTERNALS                       //
//////////////////////////////////////////////////////////
 
default
{
    link_message(integer sender_num, integer num, string str, key id) {
        integer cha = ((integer)("0x"+llGetSubString((string)llGetOwner(),-8,-1)) & 0x3FFFFFFF) ^ 0xBFFFFFFF;
        if(num != cha) return;
        string sLoad = "apikey=" + llEscapeURL(PROWL_API_KEY) + 
                        "&" + "application=" + llEscapeURL(PROWL_APPLICATION) +
                        "&" + "event=" + llEscapeURL(PROWL_EVENT) + 
                        "&" + "description=" + llEscapeURL(str);
        llHTTPRequest("https://api.prowlapp.com/publicapi/add", 
                        [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"], 
                        sLoad);
    }
    on_rez(integer num) {
        llResetScript();
    }
}