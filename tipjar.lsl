//////////////////////////////////////////////////////////
// [K] Kira Komarov - 2011, License: GPLv3              //
// Please see: http://www.gnu.org/licenses/gpl.html     //
// for legal details, rights of fair usage and          //
// the disclaimer and warranty conditions.              //
//////////////////////////////////////////////////////////
// http://wiki.secondlife.com/wiki/Tipjar
//////////////////////////////////////////////////////////

// v1 28Mar2020 laynethomas@avimail.org
//   - change notecard comment char to '#'
//   - remove tipper menu
// v2 27Jun2020 laynethomas@avimail.org
//   - Set hover text on a link with 'hover' in the description if one exists

//////////////////////////////////////////////////////////
//                   CONFIGURATION                      //
//////////////////////////////////////////////////////////
//                                                      //
string THANKS_MESSAGE = "Thank you for your tip %n%! %m% greatly appreciates it!";
string TIPPER_TOUCH_MESSAGE = "Hello %n%! If you like %m%'s work, please consider giving a tip by right-clicking the tipjar and selecting Pay and entering an ammount you would like to tip. Thank you!";
string OVERHEAD_MESSAGE = "%m%'s Tipjar, any tip is welcome!";
string LOGOUT_MESSAGE = "%m%'s Tipjar is logging off, please standby to receive your share %n%...";
key INVITE_GROUP_KEY = NULL_KEY;
string INVITE_GROUP_MESSAGE = "Please join the group %n%! To do so, please click the link in your history window (ctrl+h):";
integer DEFAULT_PAY = 51;
list PAY_BUTTONS = [23, 57, 79, 131];
integer EXCLUDE_ACCESS_FROM_SPOILS = 1;
integer TIPJAR_ROAMING = 0;
integer ROAM_INTERVAL = 30;
integer ROAM_RANGE = 10;
//                                                      //
//                  END CONFIGURATION                   //
//////////////////////////////////////////////////////////

integer hover_link = 0;

//////////////////////////////////////////////////////////
//                     INTERNALS                        //
//////////////////////////////////////////////////////////
string tokenSubstitute(string input, key id)
{
    list kSubst = llParseString2List(input, ["%"], [""]);
    integer itra;
    for(itra = 0; itra < llGetListLength(kSubst); ++itra)
    {
        if(llList2String(kSubst, itra) == "n") kSubst = llListReplaceList(kSubst, (list)llGetDisplayName(id), itra, itra);
        if(llList2String(kSubst, itra) == "m") kSubst = llListReplaceList(kSubst, (list)activeAvatarDisplayName, itra, itra);
    }
    return llDumpList2String(kSubst, "");
}

moveTo(vector position)
{
    llTargetRemove(targetID);
    targetID = llTarget(position, 0.8);
    llLookAt(position, 0.6, 0.6);
    llMoveToTarget(position, 3.0);
}

physics(integer bool)
{
    if(bool) llSetForce(<0,0,9.81> * llGetMass(), 0);
    llSetStatus(STATUS_PHYSICS, bool);
    llSetStatus(STATUS_PHANTOM, bool);
}

list_tippers(key id) {
    integer itra;
    llInstantMessage(id, "---------- BEGIN TIPPERS ----------");
    for(itra = 0; itra < llGetListLength(tippers); ++itra)
    {
        llInstantMessage(id, "secondlife:///app/agent/"+(string)llList2Key(tippers, itra) + "/im has tipped you: L$" + llList2String(tipperAmounts, itra));
    }
    llInstantMessage(id, "----------- END TIPPERS -----------");
}

list spoilMemberNames;
list spoilMemberKeys;
list spoilPercents;

list tippers;
list tipperAmounts;

list accessListNames;
list accessListKeys;

string activeAvatarName;
key activeAvatarKey;
string activeAvatarDisplayName;

key sQuery;
integer sLine;
integer readNotecard;
integer comHandleSpoiler;

integer allSpoils;

vector landingPoint;
rotation landingRotation;
list avPositions;
integer positionRoam;
integer targetID;
integer roaming;

default
{
    state_entry()
    {
        physics(FALSE);
        accessListNames = [];
        accessListKeys = [];
        sLine = 0;
        readNotecard = 0;
        activeAvatarName = "";
        activeAvatarKey = NULL_KEY;

        integer i;
        integer num_links;
        num_links = llGetNumberOfPrims();
        for (; i <= num_links; ++i) {
            list p = llGetLinkPrimitiveParams(i, [PRIM_DESC]);
            string desc = llList2String(p, 0);
            if (desc == "hover") {
                hover_link = i;
            }
        }


        llSetLinkPrimitiveParamsFast(hover_link, [PRIM_TEXT, "Tipjar loading access, please wait...", <1.0,1.0,1.0>, 1.0]);
        integer itra;
        for(itra = 0; itra < llGetInventoryNumber(INVENTORY_NOTECARD); ++itra)
        {
            if(llGetInventoryName(INVENTORY_NOTECARD, itra) == "Tipjar Access")
                jump found_access;
        }
        llSetLinkPrimitiveParamsFast(hover_link, [PRIM_TEXT, "No access list. Please revise your configuration.", <1.0,1.0,1.0>, 1.0]);
        llInstantMessage(llGetOwner(), "Failed to find Tipjar Access card. Please add a notecard called Tipjar Access and configure it apropriately.");
        return;
@found_access;
        sQuery = llGetNotecardLine("Tipjar Access", sLine);
        llSetTimerEvent(5.0);
    }

    changed(integer change)
    {
        if(change & CHANGED_INVENTORY)
            llResetScript();
    }

    on_rez(integer num)
    {
        physics(FALSE);
    }

    timer()
    {
        if(readNotecard)
        {
            llSetTimerEvent(0.0);
            llSetLinkPrimitiveParamsFast(hover_link, [PRIM_TEXT, "Tipjar idle. Please click me to activate.", <1.0,1.0,1.0>, 1.0]);
            return;
        }
        llSetTimerEvent(5.0);
    }

    dataserver(key id, string data)
    {
        if(id != sQuery) return;
        if(data == EOF)
        {
            readNotecard = 1;
            return;
        }
        if(data == "" || llSubStringIndex(data, "#") == 0) jump next_line;
        list accessParse = llParseString2List(data, ["|"], [""]);
        accessListNames += llList2String(accessParse, 0);
        accessListKeys += llList2Key(accessParse, 1);
@next_line;
        sQuery = llGetNotecardLine("Tipjar Access", ++sLine);
    }

    touch_start(integer num)
    {
        if(~llListFindList(accessListKeys, (list)llDetectedKey(0)))
        {
            activeAvatarName = llDetectedName(0);
            activeAvatarKey = llDetectedKey(0);
            activeAvatarDisplayName = llGetDisplayName(activeAvatarKey);
            activeAvatarDisplayName = "Layne Thomas";
            llOwnerSay("active display: " + (string)activeAvatarDisplayName);
            state init;
        }
    }
}

state init
{
    state_entry()
    {
        allSpoils = 0;
        tippers = [];
        tipperAmounts = [];
        spoilMemberNames = [];
        spoilMemberKeys = [];
        spoilPercents = [];
        readNotecard = 0;
        sLine = 0;
        llSetLinkPrimitiveParamsFast(hover_link, [PRIM_TEXT, "Tipjar initalizing, please wait...", <1.0,1.0,1.0>, 1.0]);
        integer itra;
        for(itra = 0; itra < llGetInventoryNumber(INVENTORY_NOTECARD); ++itra)
        {
            if(llGetInventoryName(INVENTORY_NOTECARD, itra) == "Tipjar Spoils")
                jump found_spoils;
        }
        llSetLinkPrimitiveParamsFast(hover_link, [PRIM_TEXT, "Falied! Please check Tipjar Spoils notecard.", <1.0,1.0,1.0>, 1.0]);
        llInstantMessage(llGetOwner(), "Failed to find Tipjar Spoils card. Please add a notecard called Tipjar Spoils and configure it apropriately.");
        return;
@found_spoils;
        sQuery = llGetNotecardLine("Tipjar Spoils", sLine);
        landingPoint = llGetPos();
        landingRotation = llGetRot();
        llSetTimerEvent(5.0);
    }

    changed(integer change)
    {
        if(change & CHANGED_INVENTORY)
            llResetScript();
    }

    timer()
    {
        if(readNotecard)
        {
            llSetTimerEvent(0.0);
            integer itra;
            integer percents = 0;
            for(itra = 0; itra < llGetListLength(spoilPercents); ++itra)
            {
                percents += llList2Integer(spoilPercents, itra);
            }
            if(percents > 100)
            {
                llOwnerSay("The percents in your Tipjar Spoils notecard add up to " + (string)percents + "%. They should add up to 100%. Please check your setup again.");
                state default;
            }
            llRequestPermissions(llGetOwner(), PERMISSION_DEBIT);
            return;
        }
        llSetTimerEvent(5.0);
    }

    listen(integer channel, string name, key id, string message)
    {
        if(id != activeAvatarKey) return;
 
        if(message == "[ Confirm ]")
        {
            llListenRemove(comHandleSpoiler);
            state tipjar;
        }
//        state default;
        llResetScript();
    }

    dataserver(key id, string data)
    {
        if(id != sQuery) return;
        if(data == EOF)
        {
            readNotecard = 1;
            return;
        }
        if(data == "" || llSubStringIndex(data, "#") == 0) jump next_line;
        list spoilList = llParseString2List(data, ["|"], [""]);
        if(EXCLUDE_ACCESS_FROM_SPOILS && ~llListFindList(accessListNames, (list)llList2String(spoilList, 0))) jump next_line;
        spoilMemberNames += llList2String(spoilList, 0);
        spoilMemberKeys += llList2Key(spoilList, 1);
        spoilPercents += llList2String(spoilList, 2);
@next_line;
        sQuery = llGetNotecardLine("Tipjar Spoils", ++sLine);
    }

    run_time_permissions(integer perm)
    {
        if(perm & PERMISSION_DEBIT)
        {
            integer comChannel = ((integer)("0x"+llGetSubString((string)llGetOwner(),-8,-1)) & 0x3FFFFFFF) ^ 0xBFFFFFFF;
            comHandleSpoiler = llListen(comChannel, "", activeAvatarKey, "");
            integer itra;
            string confirmText;
            for(itra = 0; itra < llGetListLength(spoilMemberNames); ++itra)
            {
                confirmText += llList2String(spoilMemberNames, itra) + " gets " + llList2String(spoilPercents, itra) + "%.\n";
            }
            confirmText += "\n\n";
            llDialog(activeAvatarKey, "Tipjar: Please revise and confirm the spoils distribution:\n\n" + confirmText, [ "[ Confirm ]", "[ Reject ]" ], comChannel);
        }
    }

}

state tipjar
{
    state_entry()
    {
        llOwnerSay("Tipjar initialized and ready to be tipped. Good Luck " + activeAvatarDisplayName + "!");
        llSetPayPrice(DEFAULT_PAY, PAY_BUTTONS);
        llSetLinkPrimitiveParamsFast(hover_link, [PRIM_TEXT, tokenSubstitute(OVERHEAD_MESSAGE, activeAvatarKey), <1.0,1.0,1.0>, 1.0]);
        if(TIPJAR_ROAMING) llSensorRepeat("", "", AGENT, ROAM_RANGE, PI, ROAM_INTERVAL);
    }

    sensor (integer num)
    {
        if(roaming) return;
        roaming = 1;
        integer itra;
        for(itra = 0, avPositions = [], positionRoam = 0; itra < num; ++itra)
        {
            avPositions += llDetectedPos(itra);
        }
        avPositions += landingPoint;
        physics(TRUE);
        moveTo(llList2Vector(avPositions, positionRoam++));
    }

    at_target(integer tnum, vector targetpos, vector ourpos)
    {
        if(tnum != targetID) return;
        if(positionRoam == llGetListLength(avPositions))
        {
            physics(FALSE);
            llSetPos(landingPoint);
            llSetRot(landingRotation);
            roaming = 0;
            return;
        }
        moveTo(llList2Vector(avPositions, positionRoam++));
    }

    touch_start(integer num)
    {
        key id = llDetectedKey(0);
 
        integer comChannel;
        if(~llListFindList(spoilMemberKeys, (list)id) || id == activeAvatarKey)
        {
            jump spoiler_touch;
        }
        llInstantMessage(id, tokenSubstitute(TIPPER_TOUCH_MESSAGE, id));
        return;
@spoiler_touch;
        comChannel = ((integer)("0x"+llGetSubString((string)llGetOwner(),-8,-1)) & 0x3FFFFFFF) ^ 0xBFFFFFFF;
        comHandleSpoiler = llListen(comChannel, "", id, "");
        llDialog(id, "Tipjar: Please choose an option:\n", [ "◆ LogOut ◆", "◆ Tippers ◆", "◆ Tops ◆", "◆ Total ◆" ], comChannel);
    }

    listen(integer channel, string name, key id, string message)
    {
        if(~llListFindList(spoilMemberNames, (list)name) || name == activeAvatarName)
        {
            jump spoiler_com;
        }
        if(message == "◆ Join ◆")
        {
            llInstantMessage(id, tokenSubstitute(INVITE_GROUP_MESSAGE, id) + "\n secondlife:///app/group/" + (string)INVITE_GROUP_KEY + "/about");
        }
        if(message == "◆ Gift ◆")
        {
            integer itra;
            list gifts;
            for(itra = 0; itra < llGetInventoryNumber(INVENTORY_OBJECT); ++itra)
            {
                gifts += llGetInventoryName(INVENTORY_OBJECT, itra);
            }
            for(itra = 0; itra < llGetListLength(gifts); ++itra)
            {
                llGiveInventory(id, llList2String(gifts, itra));
            }
        }
        return;
@spoiler_com;
        if(message == "◆ LogOut ◆")
        {
            llListenRemove(comHandleSpoiler);
            list_tippers(id);
            if(TIPJAR_ROAMING) llSensorRemove();
            if(TIPJAR_ROAMING && roaming)
            {
                physics(FALSE);
                state gohome;
            }
            state payments;
        }
        if(message == "◆ Tippers ◆")
        {
            list_tippers(id);
        }
        if(message == "◆ Tops ◆")
        {
            integer itra;
            llInstantMessage(id, "---------- BEGIN TOP TIPPERS ----------");
            for(itra = 0; itra < llGetListLength(tippers); ++itra)
            {
                if(itra == 3)
                {
                    jump end_tippers;
                }
                integer tip = llList2Integer(llListSort(tipperAmounts, 1, 0), itra);
                llInstantMessage(id, "secondlife:///app/agent/" + (string)llList2Key(tippers, llListFindList(tippers, (list)tip)) + "/im has tipped you: L$" + (string)tip);
            }
@end_tippers;
            llInstantMessage(id, "----------- END TOP TIPPERS -----------");
        }
        if(message == "◆ Total ◆")
        {
            llInstantMessage(id, "So far, " + activeAvatarDisplayName + " has made L$" + (string)allSpoils);
        }
        llListenRemove(comHandleSpoiler);
    }

    money(key id, integer amount)
    {
        if(~llListFindList(tippers, (list)id))
        {
            integer tip = amount + llList2Integer(tipperAmounts, llListFindList(tippers, (list)id));
            tipperAmounts = llListReplaceList(tipperAmounts, (list)tip, llListFindList(tippers, (list)id), llListFindList(tippers, (list)id));
            jump tippers_updated;
        }
        tippers += id;
        tipperAmounts += amount;
@tippers_updated;
        allSpoils += amount;
        llShout(PUBLIC_CHANNEL, tokenSubstitute(THANKS_MESSAGE, id));
        llInstantMessage(activeAvatarKey, llGetDisplayName(id) + " has just tipped you: L$" + (string)amount + ".");
    }
}

state gohome
{
    state_entry()
    {
        llSetLinkPrimitiveParamsFast(hover_link, [PRIM_TEXT, "Please wait, returning home...", <1.0,1.0,1.0>, 1.0]);
        physics(TRUE);
        moveTo(landingPoint);
    }
 
    at_target(integer tnum, vector targetpos, vector ourpos)
    {
        if(tnum != targetID) return;
        physics(FALSE);
        llSetPos(landingPoint);
        llSetRot(landingRotation);
        state payments;
    }
}

state payments
{
    state_entry()
    {
        llSetLinkPrimitiveParamsFast(hover_link, [PRIM_TEXT, "Loging out...", <1.0,1.0,1.0>, 1.0]);
        physics(FALSE);
        integer remSpoils = allSpoils;
        integer itra;
        for(itra = 0; itra < llGetListLength(spoilMemberKeys) && remSpoils; ++itra)
        {
            llInstantMessage(llList2Key(spoilMemberKeys, itra), tokenSubstitute(LOGOUT_MESSAGE, llList2Key(spoilMemberKeys, itra)));
            integer share = (integer)((llList2Float(spoilPercents, itra)/100.0) * (float)allSpoils);
            if(share) llGiveMoney(llList2Key(spoilMemberKeys, itra), share);
            remSpoils -= (integer)((llList2Float(spoilPercents, itra)/100.0) * (float)allSpoils);
        }
        state default;
    }
}
