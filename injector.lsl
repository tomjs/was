//////////////////////////////////////////////////////////
//   (C) Wizardry and Steamworks 2011, license: GPLv3   //
// Please see: http://www.gnu.org/licenses/gpl.html     //
// for legal details, rights of fair usage and          //
// the disclaimer and warranty conditions.              // 
//////////////////////////////////////////////////////////
 
integer comChannel = 0;
 
default
{
    touch_start(integer total_number) {
        comChannel = ((integer)("0x"+llGetSubString((string)llGetOwner(),-8,-1)) & 0x3FFFFFFF) ^ 0xBFFFFFFF;
        llListen(comChannel, "", llGetOwner(), "");
        llTextBox(llGetOwner(), "\nPlease enter a test-string to broadcast to growl-enabled devices:\n", comChannel);
    }
    listen(integer channel, string name, key id, string message) {
        llMessageLinked(LINK_THIS, comChannel, message, id);
    }
}