// map_teleporter.lsl
// Teleport to sim+location
// https://grimore.org/secondlife/map_teleporter

// Set object description to sim-name/X/Y/Z, touch to teleport
//
// Example:
//   Royal Palm/128/20/3000

// v2 - Change destination format to sim/X/Y/Z

///////////////////////////////////////////////////////////////////////////
//  Copyright (C) Wizardry and Steamworks 2012 - License: GNU GPLv3      //
//  Please see: http://www.gnu.org/licenses/gpl.html for legal details,  //
//  rights of fair usage, the disclaimer and warranty conditions.        //
///////////////////////////////////////////////////////////////////////////

default {
    touch_start(integer total_number) {
        list a = llParseString2List(llGetObjectDesc(), ["/"], []);
        if(llGetListLength(a) < 4) return;
        vector dPos = ZERO_VECTOR;
        dPos.x = llList2Float(a, 1);
        dPos.y = llList2Float(a, 2);
        dPos.z = llList2Float(a, 3);
        llMapDestination(llList2String(a, 0), dPos, ZERO_VECTOR);
    }
}
